/*******************************************************************************
 * Copyright (C) 2018 Commissariat a l'energie atomique et aux energies alternatives (CEA)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
#ifndef NOMPI
#include <mpi.h>
#else
typedef int MPI_Comm;
#endif

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/// size of the local data as [HEIGHT, WIDTH] including ghosts & boundary constants
int dsize[2];
/// size of x dimension including padding necessary for multi pipe computation on DFE
int real_dsize1;

/// 2D size of the process grid as [HEIGHT, WIDTH]
int psize[2];

/// 2D rank of the local process in the process grid as [YY, XX]
int pcoord[2];

/// the alpha coefficient used in the computation
double alpha;

/** Initialize the data all to 0 except for the left border (XX==0) initialized to 1 million
 * \param[out] dat the local data to initialize
 */
void init(double dat[dsize[0]][real_dsize1])
{
	for (int yy=0; yy<dsize[0]; ++yy)  for (int xx=0; xx<real_dsize1; ++xx)  dat[yy][xx] = 0;
	if ( pcoord[1] == 0 ) for (int yy=0; yy<dsize[0]; ++yy)  dat[yy][0] = 1000000;
}

/** Compute the values at the next time-step based on the values at the current time-step
 * \param[in]  cur  the local data at the current time-step
 * \param[out] next the local data at the next    time-step
 */
void iter(double cur[dsize[0]][real_dsize1], double next[dsize[0]][real_dsize1])
{
	int xx, yy;
	for (xx=0; xx<dsize[1]; ++xx) next[0][xx] = cur[0][xx];
	for (yy=1; yy<dsize[0]-1; ++yy) {
		next[yy][0] = cur[yy][0];
		for (xx=1; xx<dsize[1]-1; ++xx) {
			next[yy][xx] =
					  (1.-4.*alpha) * cur[yy][xx]
					+        alpha  * (   cur[yy][xx-1]
					                    + cur[yy][xx+1]
					                    + cur[yy-1][xx]
					                    + cur[yy+1][xx]
					                  ); 
		}
		next[yy][dsize[1]-1] = cur[yy][dsize[1]-1];
	}
	for (xx=0; xx<dsize[1]; ++xx) next[dsize[0]-1][xx] = cur[dsize[0]-1][xx];
}

/** Dump the first line on std output
 * \param[in]  cur  the local data at the current time-step
 */
void dump_first_line(double cur[dsize[0]][real_dsize1], int step, int rank, int n_rank, MPI_Comm comm)
{
	int xx, id;
  for(id=0 ; id < n_rank ; id++)
  {
    if(rank == id)
    {
      printf("[%02d, %02d] ", step, rank);
      for (xx=0; xx<dsize[1]; ++xx) 
        printf("%09.1f ", cur[1][xx]);

      printf("\n");
    }
#ifndef NOMPI
    MPI_Barrier(comm);
#endif
  }
}

/** Dumpathe whole array  on std output
 * \param[in]  cur  the local data at the current time-step
 */
void dump(double cur[dsize[0]][real_dsize1], int step, int rank, int n_rank, MPI_Comm comm)
{
	int xx, yy, id;
  for(id=0 ; id < n_rank ; id++)
  {
    if(rank == id)
    {
      for (yy=0; yy<dsize[0]; ++yy) 
      {
        printf("[%02d, %02d, %02d] ", step, rank, yy);
        for (xx=0; xx<dsize[1]; ++xx) 
          printf("%09.1f ", cur[yy][xx]);
        printf("\n");
      }

      printf("\n");
    }
#ifndef NOMPI
    MPI_Barrier(comm);
#endif
  }
}

#ifndef NOMPI
/** Exchanges ghost values with neighbours
 * \param[in] cart_comm the MPI communicator with all processes organized in a 2D Cartesian grid
 * \param[in] cur the local data at the current time-step whose ghosts need exchanging
 */
void exchange(MPI_Comm cart_comm, double cur[dsize[0]][dsize[1]])
{
	MPI_Status status;
	int rank_source, rank_dest;
	static MPI_Datatype column, row;
	static int initialized = 0;
	
	if ( !initialized ) {
		MPI_Type_vector(dsize[0]-2, 1, dsize[1], MPI_DOUBLE, &column);
		MPI_Type_commit(&column);
		MPI_Type_contiguous(dsize[1]-2, MPI_DOUBLE, &row);
		MPI_Type_commit(&row);
		initialized = 1;
	}
	
	// send down
	MPI_Cart_shift(cart_comm, 0, 1, &rank_source, &rank_dest);
	MPI_Sendrecv(&cur[dsize[0]-2][1], 1, row, rank_dest,   100, // send row before ghost
	             &cur[0][1],          1, row, rank_source, 100, // receive 1st row (ghost)
	             cart_comm, &status);
	
	// send up
	MPI_Cart_shift(cart_comm, 0, -1, &rank_source, &rank_dest);
	MPI_Sendrecv(&cur[1][1],          1, row, rank_dest,   100, // send column after ghost
	             &cur[dsize[0]-1][1], 1, row, rank_source, 100, // receive last column (ghost)
	             cart_comm, &status);
	
	// send to the right
	MPI_Cart_shift(cart_comm, 1, 1, &rank_source, &rank_dest);
	MPI_Sendrecv(&cur[1][dsize[1]-2], 1, column, rank_dest,   100, // send column before ghost
	             &cur[1][0],          1, column, rank_source, 100, // receive 1st column (ghost)
	             cart_comm, &status);
	
	// send to the left
	MPI_Cart_shift(cart_comm, 1, -1, &rank_source, &rank_dest);
	MPI_Sendrecv(&cur[1][1], 1, column, rank_dest,   100, // send column after ghost
	             &cur[1][dsize[1]-1], 1, column, rank_source, 100, // receive last column (ghost)
	             cart_comm, &status);
}
#endif

int main( int argc, char* argv[] )
{
	int psize_1d; 
	int pcoord_1d;
  int global_size[2], psize[2];
  int real_allocated_size;
#ifndef NOMPI
	MPI_Init(&argc, &argv);
	
	// NEVER USE MPI_COMM_WORLD IN THE CODE, use our own communicator main_comm instead
	MPI_Comm main_comm = MPI_COMM_WORLD;
	
	// load the MPI rank & size
	MPI_Comm_size(main_comm, &psize_1d);
	MPI_Comm_rank(main_comm, &pcoord_1d);
#else
	psize_1d  = 1; 
	pcoord_1d = 0;
	MPI_Comm main_comm = 0;
#endif
	
  printf("Comm size: %d", psize_1d);
	
	// set the alpha parameter, global data-size and parallelism configuration
	alpha = 0.125;
	global_size[0] = 6;
	global_size[1] = 12;
	psize[0] = 1;
	psize[1] = 1;
	
	// check the configuration is coherent
	assert(global_size[0]%psize[0]==0);
	assert(global_size[1]%psize[1]==0);
	assert(psize[1]*psize[0] == psize_1d);
	
	// compute the local data-size with space for ghosts and boundary constants
	dsize[0] = global_size[0]/psize[0] + 2;
	dsize[1] = global_size[1]/psize[1] + 2;

#ifndef NOMPI
  // create a 2D Cartesian MPI communicator & get our coordinate (rank) in it
  int cart_period[2] = { 0, 0 };
  MPI_Comm cart_comm; MPI_Cart_create(main_comm, 2, psize, cart_period, 1, &cart_comm);
  MPI_Cart_coords(cart_comm, pcoord_1d, 2, pcoord);
#endif

  real_dsize1 = dsize[1];
  real_allocated_size = sizeof(double)*real_dsize1*dsize[0];
  printf("dsize1: %d %d\n", dsize[1], real_dsize1);


  double(*cur)[real_dsize1]  = malloc(real_allocated_size);
  double(*next)[real_dsize1] = malloc(real_allocated_size);

  // initialize the data content
  init(cur);

  // our loop counter so as to be able to use it outside the loop
  int ii=0;

  // the main loop
  for (; ii<6; ++ii) {

    dump(cur, ii, pcoord_1d, psize_1d, main_comm);

    // compute the values for the next iteration
    iter(cur, next);

#ifndef NOMPI
    // exchange data with the neighbours
    exchange(cart_comm, next);
#endif

    // swap the current and next values
    double (*tmp)[real_dsize1] = cur; cur = next; next = tmp;
  }

  dump(cur, ii, pcoord_1d, psize_1d, main_comm);
	
	// free the allocated memory
	free(cur);
	free(next);
	
#ifndef NOMPI
	// finalize MPI
	MPI_Finalize();
#endif
	
	fprintf(stderr, "[%d] SUCCESS\n", pcoord_1d);
	return EXIT_SUCCESS;
}
